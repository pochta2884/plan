# Weekly Priorities - 2023

This file is meant to communicate my priorities on a weekly basis. Here's my [README file](https://gitlab.com/rayana/readme/-/blob/master/README.md).

---

## March

- [ ] [Verify Benchmark 2023 - Findings & Next Steps](https://docs.google.com/presentation/d/13MQeN7Q-kIebc9lpIUHsn4_PKEv1XklWdD2tuvZkk8M/edit#slide=id.p1)
- [ ] Summarize results https://gitlab.com/gitlab-org/gitlab-design/-/issues/2494+

### Week 10

- [x] V's career check-in
- [x] Onboard DA - ux task and update issue
    - [x] [Switchboard JTBD](https://gitlab.com/gitlab-org/ux-research/-/issues/2383)
- [ ] OKR - review JTBDs https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/102+
- [ ] Actionable insight workshop Conversational AI https://gitlab.com/gitlab-org/ux-research/-/issues/2901#note_1791541307
- [ ] https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/78+
- [x] UX review with Gina
- [x] https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/108+

## February

### Week 9

- [x] Onboard DA 
    - [ ] Support Emily
- [ ] [Switchboard JTBD](https://gitlab.com/gitlab-org/ux-research/-/issues/2383)
- [x] [Borrow req](https://gitlab.com/gitlab-com/Product/-/issues/13128) 
- [ ] https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/78+
- [x] G's IGP check in 
- [x] Coverage issue post Summit
- [x] UXR alignment Karen

### Week 8

- [x] [Finalize DA onboarding plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/100)
- [x] V's career check-in
- [x] E's UX showcase prep
- [x] Review FY25 product direction / [quarterly quickoff](https://university.gitlab.com/learn/video/fy25-q1-quarterly-kickoff)
- [x] [FY24 Q4 Switchboard customer demo insights](https://docs.google.com/presentation/d/1UoAJaDQnZup0TK3zqK0vaKhcAwERiR61dZfPvuEMEKo/edit#slide=id.g12b319f6181_0_0)
- [ ] [Switchboard JTBD](https://gitlab.com/gitlab-org/ux-research/-/issues/2383)
- [x] [Team skill mapping](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2483) | https://gitlab.com/gitlab-org/gitlab-design/-/issues/2494+
- [x] FY25 CICD usability improvements ([slack](https://gitlab.enterprise.slack.com/archives/C042XA6VA9E/p1707843638022719))

### Week 7

- [x] [DA onboarding plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/100)
- [x] OKRs
- [x] https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/97+
- [x] [IGP FY25](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/99)
- [ ] Review FY25 product direction
- [x] https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/1905+
- [x] UX call agenda update

### Week 6

- [x] Hiring
    - [x] Ref checks
    - [x] Justification
    - [x] [Weekly team update](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/2109#note_1660742460)
- [x] IGP prep https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/97+
- [ ] DA Onboarding - ux plan
- [x] OKR review


## January

### Week 5

(my god, how can it STILL be January?)

- [x] Hiring
    - [x] Interviews 0/0
    - [x] Submit scorecards 1/1
    - [x] Sync with Riley
    - [x] Ref check prep
    - [x] Review applications
    - [x] [Weekly team update](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/2109#note_1660742460)
- [ ] ~[OKR: UX Scorecard](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4490)~
- [x] Finalize TA prep 4/4
- [x] [Talent assessment results](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2385)
- [x] G's career dev check in
- [x] [V's IGP](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/75#note_1688665160)

### Week 4

- [ ] Hiring
    - [ ] Interviews 2/3
    - [ ] Submit scorecards 2/3
    - [ ] [Weekly team update](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/2109#note_1660742460)
- [ ] [OKR: UX Scorecard](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4490)
- [x] CI Direction think big
- [x] Merge trains competitor eval https://www.youtube.com/watch?v=eUxNVmi81d8
- [x] Register for Summit

### Week 3

- [x] Hiring
    - [x] Hiring manager tasks
    - [x] Review role req with team
    - [x] Weekly team update
- [x] [Annual comp review](https://handbook.gitlab.com/handbook/total-rewards/compensation/compensation-review-cycle): Submit slates in Workday (due 2024-01-16 @ 5pm PT)
- [x] [OKR: UX Scorecard](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4490) - pick jtbd
- [x] Showcase schedule
- [ ] Register for Summit

### Week 2

- [x] Hiring tasks
- [x] Switchboard role debrief & sync with Riley [doc](https://docs.google.com/document/d/1CEvUJoq7VbkCBTCekJPuaI4XrDM1v08Ze-CAZHiQKdQ/edit)
- [ ] Triage reports
- [ ] [Annual comp review](https://handbook.gitlab.com/handbook/total-rewards/compensation/compensation-review-cycle)
    - [ ] Submit slates in Workday (due 2024-01-16 @ 5pm PT)
- [x] Plan Talent assessment communication (due 2024-02-09)
- [ ] Register for Summit
- [x] Finalize ux Showcase schedule

### Week 1

- [x] Hiring tasks - candidate outreach, riley
- [x] Review Bonnie's milestone plan
- [x] UX Showcase schedule https://gitlab.com/gitlab-org/gitlab-design/-/issues/2448
- [x] Triage reports
