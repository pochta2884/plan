# Quarterly Goals

This content is meant to communicate how I intend to allocate my time. I review it monthly, but it should largely remain consistent on the time-scales of quarters.

As of October 2020, this is also the high-level planning and execution doc for [Acting](https://about.gitlab.com/handbook/people-group/promotions-transfers/#acting) [Product Design Manager](https://about.gitlab.com/job-families/engineering/ux-management/) - CI/CD. These goals should be people and process focused, and eventually feed back into my personal development and OKRs plan.

* Period: October 2020 - TBD.
* Time distribution: 50%

## Scope & Balance

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| **Product Design Manager (acting)** | [CDF Reviews](https://about.gitlab.com/handbook/engineering/career-development/), Career Development, Team Engagement, GitLab Values coaching, interviewing, hiring | 50% |
| **Staff Product Designer** |  | 35% |
| - Sensing Mechanisms | Internal context gathering, customer interviews, analyst inquiries, competitive review | _10%_ |
| - Cross Section Product Experience | Think Big-Think Small, Walk-throughs, learning goals, direction content review | _5%_ |
| - Design for Testing | Tactical design, Team Engagement | _10%_ |
| - Design for Runner | Tactical design, Team Engagement | _10%_ |
| **Personal Growth / Leadership Opportunities** | Career Development, Pajamas Maintainer, Representing GitLab externally, Representing Design internally | 10% |
| **Engagement** | Coffe chats, sync and async cross-functional engagements | 5% |

## Weekly Priorities

Each week, I create a [markdown file](https://gitlab.com/rayana/plan/-/tree/master/tasks/2020) to communicate my priorities on a weekly basis.

## Piorities as Product Design Manager

* Continue fostering a safe space for the CI/CD Design team, where they’re comfortable sharing feedback and advocating for change they see as necessary. 
* Hold regular 1:1s with all members of the design team.
* Hiring PD and PDM.
* Continue progress on individual growth plan.
* Align with leadership on quarterly goals and help instrument them with the team.
* Continue efforts to encourage cross-functional collaboration and iteration between Product, Engineering, and UX.
  * Follow up on initiatives around getting UX in CI/CD to be more effective, more efficient, and more results oriented across the board.
* When necessary, provide guidance and review UX deliverables to ensure high-quality output.
* Setting up meetings with PMs and EMs.

## Priorities as Staff Product Designer

* Transition all Release Management design work to new DRIs.
* Onboard Verify Testing & Runner.
* Whenever possible, support PMs on problem/solution validation and customer interviews.
* Whenever possible, join customer calls.

## Deprioritize to make room 

* Settings UX https://gitlab.com/groups/gitlab-org/-/epics/3535
  * Now that we have a clear DRI for Settings/Navigation (Create), I'm handing Settings UX over to Michale Le, while still acting as advisor when necessary.
* Joining working groups and other general company calls.

## Stop doing to make room

* Onboarding/trainee buddy (besides Amelia).
* Handbook MRs outside of CI/CD UX.
* UX MRs outside of the Release Management scope.
* Initiate updates to/create new Pajamas components.
