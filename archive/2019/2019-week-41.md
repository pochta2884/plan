# Week 41 | October 7 to 1

* Working Milestone: `12.4`
* Planning Milestone: `12.5`

## What's keeping me busy

- Design OKRs
- Catching up after PTO, Release 12.4 issues, Pajamas MRs, milestone planning

### Tasks

#### UX

- [x] Review lists MR https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/merge_requests/1582
- [x] Review accordion MR https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/merge_requests/1575
- [ ] Review Table styles https://gitlab.com/gitlab-org/gitlab-ui/issues/452
- [x] Review tooltip/popover mr https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/merge_requests/1539

#### Release
[**12.4 Release issue board**](https://gitlab.com/groups/gitlab-org/-/boards/1213499?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=12.4&label_name[]=Release&label_name[]=UX)
- [ ] [Improve detached pipeline - UX Research](https://gitlab.com/gitlab-org/ux-research/issues/244)
    - [ ] Compile results - https://gitlab.com/gitlab-org/ux-research/issues/386
- [ ] Solution Validation for Releases https://gitlab.com/gitlab-org/gitlab/issues/31615 - [Q3 UX OKR](https://gitlab.com/gitlab-org/gitlab-design/issues/527)
    - [ ] Review MR https://gitlab.com/gitlab-org/gitlab/merge_requests/17278
    - [ ] Potentially review MR for https://gitlab.com/gitlab-org/gitlab/issues/26001
- [x] Environments dashboard - review MR with Andrew https://gitlab.com/gitlab-org/gitlab/merge_requests/18280
  
[**12.5 Release issue board**](https://gitlab.com/groups/gitlab-org/-/boards/1276625?scope=all&utf8=✓&state=opened&milestone_title=12.5&label_name[]=devops%3A%3Arelease&label_name[]=UX&label_name[]=direction)
- [ ] Reply to issue herder https://issue-herder.herokuapp.com/
- [ ] Organize issues related to Releases, for planning. Prioritize them with Orit.
- [x] Kickoff planning
- [ ] Start looking into the Releases/Tags relationship for future planning https://gitlab.com/groups/gitlab-org/-/epics/764
 
#### Misc
- [ ] Team label process https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/28089
- [ ] Clean-up my mail inbox. Goal: 0 unread by the end of the week

### Retrospective

...
