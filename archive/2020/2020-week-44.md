# Week 44 | Oct 26 - 30

## General

- [ ] [Read about performance factors](https://about.gitlab.com/handbook/people-group/performance-assessments-and-succession-planning/)
- [ ] Fill in my performance factor sheet

## Acting PDM

- [ ] Nadia's transition issue https://gitlab.com/gitlab-org/gitlab/-/issues/263140
- [ ] Acting manager tasks https://gitlab.com/gitlab-com/people-group/Training/-/issues/927
- [x] Schedule 1:1s

### [Verify:Continuous Integration](https://about.gitlab.com/handbook/engineering/development/ops/verify/continuous-integration/) (Veethika)

**Covering for [Verify:Testing](https://about.gitlab.com/handbook/engineering/development/ops/verify/testing/) | [Verify:Runner](https://about.gitlab.com/handbook/engineering/development/ops/verify/runner/)**

- [ ] CI UX DOD https://gitlab.com/gitlab-org/verify-and-release-ux-team/-/issues/16

### [Verify:Pipeline Authoring](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-authoring/) (Nadia)

- [ ]

### [Ops:Package](https://about.gitlab.com/handbook/engineering/development/ops/package/) (Iain)

- [ ]

### Release:Progressive Delivery (Dimitrie)

- [ ] Transition issue https://gitlab.com/gitlab-org/gitlab/-/issues/270425

---

## Release Management

- [ ] **NEED UX REVIEW ITEMS** https://gitlab.com/groups/gitlab-org/-/epics/2439
- [ ] 13.5 retro https://gitlab.com/gl-retrospectives/release-stage/release-management/-/issues/10

## GitLab Design

- [ ] https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/1653
- [x] Pedro/Taurie maintainer areas
  - [x] share feedback from Juan
