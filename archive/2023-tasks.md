# Weekly Priorities - 2023

This file is meant to communicate my priorities on a weekly basis. Here's my [README file](https://gitlab.com/rayana/readme/-/blob/master/README.md).

---

## December

### Week 52

OOO

### Week 51

:island: OOO December 18, 19

1. [x] Hiring tasks
1. [x] G's IGP

### Week 50

1. Hiring
    1. [x] Interviews 2/2
    1. [x] Submit scorecards 2/2
    1. [ ] Ref check 1/3
    1. [ ] Provide status update
1. [x] BT UX onboarding https://gitlab.com/gitlab-org/gitlab-design/-/issues/2411+
1. [X] Workday calibration (final) Dec 11-22
1. [x] [Review UX showcase - emily](https://docs.google.com/presentation/d/1pljCtXEe2jPh3UBalWuYoA3qWTmwwSp-4odaT5CrKKM/edit#slide=id.g2a28f94d056_0_15)
1. [ ] Fix expense report expenses@gitlab

### Week 49

1. Hiring
    1. [ ] ~Interviews~
    1. [ ] ~Submit scorecards~
    1. [x] Sync with Riley
    1. [x] Provide status update
1. [ ] BT UX onboarding https://gitlab.com/gitlab-org/gitlab-design/-/issues/2411+
1. [x] Backup loom vids
1. [ ] [Review UX showcase - emily](https://docs.google.com/presentation/d/1pljCtXEe2jPh3UBalWuYoA3qWTmwwSp-4odaT5CrKKM/edit#slide=id.g2a28f94d056_0_15)
1. [x] OKR Review https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4493+
1. [x] Switchboard borrow

## November

### Week 48

:island: OOO November 27, 28, and December 1st.

1. Hiring
    1. [ ] ~Interviews~
    1. [ ] ~Submit scorecards~
    1. [ ] Sync with Riley
    1. [ ] Provide status update
1. [x] BT onboarding https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/5906

### Week 47

🕓 Time zone this week is GMT-3 https://time.is/Recife

1. Hiring
    1. [x] Interviews 2/2
    1. [x] Submit scorecards 2/2
    1. [x] Sync with Riley
    1. [x] Provide status update
1. [x] Triage reports
1. [x] BT onboarding 

### Weekk 46

:island: OOO Nov 14-17 https://gitlab.com/gitlab-org/gitlab-design/-/issues/2398+

1. Hiring tasks
    1. [ ] Weekly update to Loryn/Amy
1. [x] TA - Calibration

### Week 45

1. Weekly
    1. [x] ~triage reports~
1. Hiring tasks
    1. [ ] ~Interviews~
    1. [x] Review/submit scorecards
    1. [x] Check in with Riley
    1. [ ] Weekly update to Loryn/Amy
1. [x] Talent assessment 4/4
1. [x] Create coverage issue for upcoming PTO https://gitlab.com/gitlab-org/gitlab-design/-/issues/2398+

## October

### Week 44

1. Weekly
    1. [x] triage reports
1. Hiring tasks
    1. [x] Review applications - day 4/5
    1. [x] Interviews
    1. [x] Review/submit scorecards
    1. [x] Check in with Loryn
1. [ ] Talent assessment 0/4
1. [x] IGP check in Gina
1. [x] https://gitlab.com/gitlab-org/ux-research/-/issues/2766+

### Week 43

1. Weekly
    1. [ ] ~triage reports~
1. Hiring tasks
    1. [x] Review applications - day 5/5
    1. [ ] ~Interviews~
    1. [x] Review/submit scorecards
    1. [x] Sync with Riley
1. [ ] Talent assessment 0/4
1. [x] Self assessment

### Week 42

🏙️ In Berlin for the Product Team Event (Tuesday - Thursday) this week.

1. Weekly
    1. [ ] ~triage reports~
    1. [x] [OKRs check in](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3561)
1. Hiring tasks
    1. [x] References BT 3/3
    1. [x] Communicate with EM/PM for PS
1. [ ] ~[Switchboard UX](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/138)~

### Week 41

1. Weekly
    1. [x] triage reports
    1. [x] [OKRs check in](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3561)
1. Hiring tasks
    1. [x] Sync with Riley
    1. [x] Review applications
    1. [x] Interviews 3/3
    1. [x] Complete scorecards 3/3
    1. [x] Justification BT
    1. [x] References BT 2/3
1. [ ] [Switchboard UX](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/138)
1. [x] UX Showcase prep
1. [x] Talent assessment - create tracking issue and share files w the team https://gitlab.com/gitlab-org/gitlab-design/-/issues/2385+

### Week 40

1. Weekly
    1. [x] triage reports
    1. [x] [OKRs check in](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3561)
1. Hiring tasks
    1. [ ] Sync with Riley
    1. [x] Review applications
    1. [ ] ~Interviews~
    1. [ ] ~Complete scorecards~
1. [ ] Switchboard UX
1. [x] US visa application

## September

### Week 39

ℹ️ Canceling/moving calls to async this week so that I can focus on Switchboard GA on Friday.

1. Weekly
    1. [x] triage reports
    1. [X] [OKRs check in](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3561)
1. [x] Elevate exam on Tuesday
1. [x] Complete elevate tasks
1. Switchboard UX
    1. [x] https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/138+
1. Hiring tasks
    1. [x] Sync with Riley
    1. [x] Review applications
    1. [x] Interviews
    1. [x] Complete scorecards
1. [x] PE PM interviews 

### Week 38

ℹ️ Canceling/moving calls to async this week so that I can focus on Switchboard GA.

1. Weekly
    1. [x] triage reports
    1. [x] [OKRs check in](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3561)
1. [x] Elevate project https://gitlab.com/gitlab-com/people-group/learning-development/programs/-/issues/60+
1. Switchboard UX
    1. [x] TW sync call
    1. [ ] https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/138+
1. Hiring tasks
    1. [x] Switchboard
        1. [x] Complete scorecard from last week
        1. [ ] ~Interviews~
    1. [x] Pipeline Security

### Week 37

1. [ ] Weekly: triage reports
1. [x] [OKRs check in](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3561)
1. [x] Hiring tasks
    1. [x] Switchboard
        1. [x] Complete scorecard from last week
        1. [x] Interviews
    1. [x] Pipeline Security
1. [x] Elevate project https://gitlab.com/gitlab-com/people-group/learning-development/programs/-/issues/60+
    1. [x] Kickoff call
    1. [x] Async project
    1. [x] Dry run with Agnes on Friday  
1. [ ] Switchboard UX
    1. [ ] Apply Tenant changes in Maintenance window https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/152#note_1546944157
    1. [ ] Configuration UI - log issues
    1. [ ] ~Root credentials page~
    1. [ ] Log in styling https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2364#note_1544535471
1. [x] Career development call w/ Gina
1. [x] [360 feedback](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/86)

### Week 36

1. [x] Weekly: triage reports
1. [x] [OKRs check in](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3561)
1. [x] Hiring tasks
    1. [x] Switchboard
    1. [x] Pipeline Security
1. [ ] Switchboard UX
    1. [x] Configuration UI assessment
    1. [ ] Apply Tenant changes in Maintenance window https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/152#note_1546944157
1. [x] [360 feedback](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/86)
1. [x] Elevate training
1. [x] [V's transtion plan](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2347)

## August

### Week 35

🤒 Out sick August 28-30

1. [x] Weekly: triage reports
1. [x] Hiring tasks
    1. [x] Switchboard
    1. [x] Pipeline Security
1. [x] 360 feedback - by Friday

### Week 34

🏝️ OOO between August 23-25

1. [x] Weekly: triage reports
1. [x] Hiring tasks
    1. [x] Switchboard
    1. [x] PE debrief
    1. [x] PS comms plan
    1. [x] PS Kickoff
1. [x] Switchboard
    1. [x] [UX Delivery plan](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/138#current-ux-priorities)
1. [ ] Runner minutes https://gitlab.com/gitlab-org/gitlab/-/issues/421457#note_1518287184
1. [x] Gina's support req 

### Week 33

1. [x] Weekly: triage reports
1. [ ] Hiring tasks
    1. [ ] Switchboard
    1. [x] Review Service desk applicants and make decision
    1. [ ] PE debrief
1. [x] Q3 OKRs https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4089
1. [ ] Switchboard
    1. [ ] [UX Delivery plan](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/138#current-ux-priorities)
    1. [x] MRs https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/switchboard/-/merge_requests/709#note_1517488587

### Week 32

1. [x] Weekly: triage reports
1. [x] Q3 OKRs https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4089
1. [ ] Switchboard
    1. [ ] ~Draft - UX blog post~
    1. [x] [Delivery plan](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/138#current-ux-priorities)
    1. [ ] Labels/PDFlow issue
    1. [x] JTBD with Michael
    1. [x] landing flow
1. [ ] Update Gina's [specialty](https://gitlab.slack.com/archives/C03MSG8B7/p1689762321832869?thread_ts=1689761535.715189&cid=C03MSG8B7)
1. [ ] From Deploy: Watch interview switchboard https://gitlab.dovetailapp.com/data/Andy-Service-list-solution-validation-7yAT8lnbD7Jyitnnick1Bd
1. [x] Reply to erika https://gitlab.slack.com/archives/CL9STLJ06/p1691116374548879
1. [x] Expense reports
1. [x] Elevate 

### Week 31

1. [x] Weekly: triage reports
1. [x] Elevate
1. [ ] Switchboard
    1. [x] Coverage plan https://gitlab.com/gitlab-org/gitlab-design/-/issues/2325+
    1. [x] Prep for C1 Switchboard interview prep https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2425
    1. [x] Email notifications https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2404+
    1. [x] [Job requirements](https://docs.google.com/document/d/1CEvUJoq7VbkCBTCekJPuaI4XrDM1v08Ze-CAZHiQKdQ/edit)
    1. [ ] Labels/PDFlow issue
    1. [x] Priorities tracker -> https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/138#current-ux-priorities
1. [x] SUS https://gitlab.com/gitlab-org/ux-research/-/issues/2579+
1. [ ] Update Gina's [specialty](https://gitlab.slack.com/archives/C03MSG8B7/p1689762321832869?thread_ts=1689761535.715189&cid=C03MSG8B7)
1. [ ] From Deploy: Watch interview switchboard https://gitlab.dovetailapp.com/data/Andy-Service-list-solution-validation-7yAT8lnbD7Jyitnnick1Bd
1. [x] Q3 KRs https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3561#note_1494469775

## July

### Week 30

1. [x] Weekly: triage reports
1. [x] Switchboard backfill plan https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2523
1. [x] Switchboard coverage plan https://gitlab.com/gitlab-org/gitlab-design/-/issues/2325
1. [x] Mid-year check in V
1. [x] DB PB VM
1. [ ] Update Gina's [specialty](https://gitlab.slack.com/archives/C03MSG8B7/p1689762321832869?thread_ts=1689761535.715189&cid=C03MSG8B7)
1. [ ] ~Emily's blog post https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34383+~
1. [x] UX showcase
1. [ ] From Deploy: Watch interview switchboard https://gitlab.dovetailapp.com/data/Andy-Service-list-solution-validation-7yAT8lnbD7Jyitnnick1Bd
1. [ ] Prep for C1 Switchboard interview prep https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2425

### Week 29

🤒 July 17

1. [x] triage reports
1. [x] offboarding tasks https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/5596
1. [x] mid-year check in https://gitlab.com/gitlab-org/gitlab-design/-/issues/2321
1. [x] mid-year check in ci/cd https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/80
1. [x] elevate training
1. [ ] DB VM
1. [x] mid year review prep ([EB](https://docs.google.com/document/d/16Fr4trnf8j6DFzyP_q0-cAJuM9dV85Beq9Bl7NT5NkE/edit#heading=h.mwom699moq2w))

### Week 28

🏝️ OOO July 10-12

1. [x] triage reports
1. [x] coaching updates
1. [ ] mid-year check in https://gitlab.com/gitlab-org/gitlab-design/-/issues/2321
1. [ ] ~elevate training~ 
1. [ ] DB VM
1. [ ] mid year review prep ([EB](https://docs.google.com/document/d/16Fr4trnf8j6DFzyP_q0-cAJuM9dV85Beq9Bl7NT5NkE/edit#heading=h.mwom699moq2w))

### Week 27

🏝️ OOO July 3, July 7

1. [x] Triage reports 
1. [x] Career development conversations and review https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/73
1. [x] Coaching updates
1. [x] [Elevate module 3](https://levelup.gitlab.com/learn/course/module-3-developing-others-pre-work/developing-others-asynchronous-work/coaching-cohort-pre-work?client=internal-team-members&page=2)
1. [ ] DB VM
1. [ ] mid year review prep ([EB](https://docs.google.com/document/d/16Fr4trnf8j6DFzyP_q0-cAJuM9dV85Beq9Bl7NT5NkE/edit#heading=h.mwom699moq2w))
1. [x] [review emily's ux showcase](https://docs.google.com/presentation/d/1Ot5398iNACvWmYVZ5pltI0MB09RDKtzYo7CrrM9N8H0/edit?usp=sharing)
1. [ ] https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2403+

## June

### Week 26

🏝️ OOO Jun 29 - 30

1. [x] Triage reports 
1. [x] Career development conversations and review https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/73
1. [x] Coaching updates
1. [ ] [Elevate module 3](https://levelup.gitlab.com/learn/course/module-3-developing-others-pre-work/developing-others-asynchronous-work/coaching-cohort-pre-work?client=internal-team-members&page=2)
1. [x] [mid year check in prep](https://about.gitlab.com/handbook/people-group/talent-assessment/#mid-year-check-in)
1. [ ] DB VM

### Week 25

1. [x] Triage reports 
1. [x] Career development conversations and review https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/73
1. [x] Coaching updates
1. [ ] Switchboard UX
1. [ ] [mid year check in prep](https://about.gitlab.com/handbook/people-group/talent-assessment/#mid-year-check-in)
1. [x] CI catalog design sync
1. [ ] [Elevate module 3](https://levelup.gitlab.com/learn/course/module-3-developing-others-pre-work/developing-others-asynchronous-work/coaching-cohort-pre-work?client=internal-team-members&page=2)

### Week 24

1. [ ] Triage reports 
1. [x] Elevate training
1. [ ] Elevate sign up (by Friday 16) https://gitlab.com/gitlab-com/people-group/learning-development/programs/-/issues/30 
1. [x] Career development conversations and review https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/73
1. [x] Coaching updates
1. [ ] Switchboard UX

### Week 23

🏝️ OOO June 9

1. [ ] ~Engagement survey~
1. [x] Weekly: Triage reports
1. [x] Switchboard UX
1. [ ] ~Design sprint prep https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/21~
1. [x] Elevate training

## May

### Week 22

🎡 OOO May 29

1. [x] Weekly: Triage reports
1. [ ] Switchboard UX
    1. [ ] https://www.figma.com/file/YaJQT4WARRk9u0YQveioPJ/Switchboard?type=design&node-id=1254%3A80612&t=zfJDyXDypjNeKNA2-1
1. [x] Coaching doc
1. [x] IGP tracker
1. [x] Gina's growth & dev
1. [x] DB GD
1. [ ] Design sprint prep https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/21

### Week 21

🌞 FF day, May 26

1. [x] Weekly: Triage reports
1. [x] Monthly: Ops PI review - https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/merge_requests/2847+
1. [x] Doc coach
   1. [x] team update 
   1. [x] email
1. [x] GD DB
1. [x] B DB
1. [ ] Switchboard UX   
    1. [ ] Review https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2085
1. [x] UX weights & comms plan

### Week 20

1. [x] Switchboard UX
1. [ ] Doc coach
1. [x] Triage reports
1. [ ] ~Review Gina's research plan https://gitlab.com/gitlab-org/ux-research/-/issues/2403~
1. [x] review runner ui 
1. [ ] GD DB
1. [x] Elevate
1. [x] V's speaking req

### Week 19

1. [x] Switchboard UX
1. [x] Triage reports
1. [ ] ~Erika's research support~ https://gitlab.slack.com/archives/CL9STLJ06/p1682622377678209
1. [x] V's Growth and development https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/71 
1. [x] [Elevate pre-work](https://levelup.gitlab.com/learn/learning-path/elevate)
1. [x] Elevate Module 1: Step 3 - Group Coaching
1. [x] Review PE UX issues
1. [ ] Review Gina's research plan by Friday https://gitlab.com/gitlab-org/ux-research/-/issues/2403
1. [ ] GD DB

### Week 18

:island: OOO May 5

1. [x] Switchboard UX
1. [x] V's Growth and development https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/71 
    1. [ ] update goals
1. [ ] Erika's research support https://gitlab.slack.com/archives/CL9STLJ06/p1682622377678209
1. [x] Triage reports
1. [x] get team to sync with gcal https://about.gitlab.com/handbook/product/ux/how-we-work/#syncing-pto-by-deel-with-google-calendar
1. [x] environments - get survey ready for conference - Emily
1. [x] career dev template updates
1. [x] ci/cd ux review call

## April

### Week 17

PTO Apr 27 - 28

- [x] AI Verify Deploy
- [ ] Switchboard UX
    - [ ] research plan https://gitlab.com/gitlab-org/ux-research/-/issues/2400#note_1325719059
    - [x] ux feedback https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2008/
    - [x] design review onboarding flow
    - [x] ux planning https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2016
    - [ ] https://www.youtube.com/watch?v=2_EZYe5wilQ2dMjKVVY-0
    - [x] ux showcase https://www.youtube.com/watch?v=9OocRsu9kjI
    - [x] pm & em chat (schedule)
    - [ ] scorecard https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/1565#note_1320648112
- [ ] Triage reports
- [x] Ops KPI review prep https://gitlab.com/gitlab-com/Product/-/issues/5639
- [ ] ux theme feedback https://gitlab.com/gitlab-org/gitlab-design/-/issues/2287
- [x] CI/CD UX Prep
- [x] SUS verbatim https://gitlab.com/gitlab-org/ux-research/-/issues/2452

### Week 16

PTO Apr 19-21

- [x] ux delivery plan switchboard (doc)
- [ ] ux theme feedback https://gitlab.com/gitlab-org/gitlab-design/-/issues/2287
- [x] equity ref updates 0/1
- [x] https://gitlab.com/gitlab-com/Product/-/issues/5639+
- [x] Triage reports
- [x] AI support https://gitlab.com/gitlab-com/Product/-/issues/5649+
- [x] Auth UX support
- [x] elevate training

### Week 15

PTO April 10

- [x] AI Verify Deploy
- [ ] Switchboard UX
    - [ ] research plan https://gitlab.com/gitlab-org/ux-research/-/issues/2400#note_1325719059
    - [ ] https://www.youtube.com/watch?v=2_EZYe5wilQ
    - [x] design feedback https://www.figma.com/file/YaJQT4WARRk9u0YQveioPJ/Switchboard?node-id=0-1&t=zHxbqVaJ2dMjKVVY-0
    - [ ] pm & em chat (schedule)
    - [ ] scorecard https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/1565#note_1320648112
- [x] ux themes prep
- [x] ux themes training
- [x] ci/cd ux prep 
- [x] complete triage reports
- [x] tufts ux weekly
- [x] Pipeline Security UX scope
- [x] ci/cd ux review 

### Week 14

PTO April 7

**From last week**
- [x] Answer Pedro's prioritizarion questions
- [ ] Complete triage reports

**This week**
- [x] Switchboard UX
- [x] feedback on runner fleet research plan https://docs.google.com/document/d/1Jwc3NveoISPB6R2nwIE0c4BGA7v2rtQHGexQf5NaBdc/edit#heading=h.qqm6fsi58dx1
- [ ] ~~create coverage~~
- [x] equity ref updates 3/4
- [x] review catalog nav discussion https://gitlab.com/gitlab-org/ux-research/-/issues/2401

## March

### Week 13

**From last week**
- [x] Answer Pedro's prioritizarion questions
- [x] KPIs Update [internal handbook](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/#ops-section---sus-overview) ops sus 
- [ ] Complete triage reports

**This week**
- [x] Switchboard UX plan
- [x] UX showcase swap https://gitlab.com/gitlab-org/gitlab-design/-/issues/2231+
- [x] Performance review Ops prep
- [x] Ops PI review
- [ ] feedback on runner fleet research plan https://docs.google.com/document/d/1Jwc3NveoISPB6R2nwIE0c4BGA7v2rtQHGexQf5NaBdc/edit#heading=h.qqm6fsi58dx1

### Week 12

Conference from March 22-26

**From last week**
1. [ ] Answer Pedro's prioritizarion questions
1. [ ] KPIs Update [internal handbook](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/#ops-section---sus-overview) ops sus 
1. [ ] Complete triage reports

**This week's priorities**
- [x] https://gitlab.com/gitlab-org/ux-research/-/issues/2401+
- [ ] Switchboard jtbd
- [x] Q2 OKR planning

### Week 11

**From last week**
1. [x] Update CI/CD UX handbook pages https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
1. [ ] Answer Pedro's prioritizarion questions
1. [ ] KPIs Update [internal handbook](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/#ops-section---sus-overview) ops sus 

**This week's priorities**
1. [x] JiHu alignment sync
1. [x] Switchboard research plan https://gitlab.com/gitlab-org/ux-research/-/issues/2392
1. [x] Switchboard scorecard w Ali
1. [x] Switchboard onboarding tasks https://gitlab.com/gitlab-org/gitlab-design/-/issues/2232
1. [x] CI/CD Team meeting prep
1. [x] Release post: triage usability issues https://gitlab.slack.com/archives/CPQT50BFG/p1678406506925439
1. [ ] Complete triage reports
1. [x] workday: see if everyone has specialty added

### Week 10

1. [x] PTO catch-up - inbox clean-up
1. [ ] Update CI/CD UX handbook pages https://gitlab.com/gitlab-org/gitlab-design/-/issues/2238
1. [x] UX showcase prep https://gitlab.com/gitlab-org/gitlab-design/-/issues/2231
1. [x] Switchboard - learning plan https://gitlab.com/gitlab-org/gitlab-design/-/issues/2232
1. [x] Configure - learning plan 
1. [x] OKRs progress update
1. [x] Switchboard deep dive with Fabian
1. [x] Triage reports
1. [x] V's MR https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/120765
1. [x] Update Release/Configure jtbd https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/121004
1. [x] Switchboard foundation research https://gitlab.com/gitlab-org/ux-research/-/issues/2392
1. [ ] Answer Pedro's prioritizarion questions
1. [ ] Update [internal handbook](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/#ops-section---sus-overview) ops sus 

### Week 9

PTO Feb 27-28 https://gitlab.com/gitlab-org/gitlab-design/-/issues/2192

1. [x] PTO catch-up - inbox clean-up
1. [x] Switchboard - schedule coffee chats
1. [x] MRs review https://gitlab.com/dashboard/merge_requests?reviewer_username=rayana
1. [x] Configure - EM chat

## February

### Week 8

PTO https://gitlab.com/gitlab-org/gitlab-design/-/issues/2192

### Week 7

PTO Feb 17

1. [x] Manager transition prep and meeting
1. [x] ali's transition issue
1. [x] emily's transition issue
1. [x] Reorg updates and comm
1. [ ] [Package reorg](https://docs.google.com/document/d/11O4JoIw3LqZpZ-XncKD6-RR9jD6DWuxuX5F4Nwqlgm4/edit)
1. [ ] Ops KPI review and prep
1. [x] Q1 OKRs review 
1. [x] Review Ops direction updates https://gitlab.com/gitlab-com/Product/-/issues/5332
1. [x] Verify benchmarking tasks
1. [x] SUS review issue https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/69

### Week 6

PTO Feb 7-8

1. [x] OKR review
    1. [x] Verify
    1. [x] Package
    1. [x] Release
1. [x] Communicate comp and role changes 
1. [x] prep: SUS verbatim review w/ team
1. [x] Design reviews https://gitlab.com/gitlab-org/gitlab-design/-/issues/2203
1. [x] laptop wipe!
1. [ ] ~[interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/2277) training https://www.youtube.com/watch?v=b03eiIwz2LE~
1. [x] 10 design-complete issues for the Contributor Success team https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/122387193

## January

### Week 5

1. [x] Prep for pto; update calendar
1. [ ] SUS verbatim review w/ team
1. [x] S1/S2 SUS-impacting issues triage and recommendations
1. [x] Design review sync prep
1. [x] Tufts project tasks https://gitlab.com/tufts-university/tufts-university-capstone/
1. [x] Review Q1 OKRs
1. [x] Communicate comp changes

### Week 4

1. [x] setup tufts project https://gitlab.com/tufts-university/tufts-university-capstone/-/issues/5
1. [ ] [review sj's async session plan](https://docs.google.com/document/d/1fDOOJYNv-0FvV1J5KEGypckXqckYapac4Wb8PaWNiRc/edit?pli=1)
1. [x] Ops SUS PI MR https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/merge_requests/2081/
1. [x] [Talent Assessment KM](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2122)
1. [x] Follow OKRs discussion https://gitlab.com/gitlab-com/Product/-/issues/5122
1. [x] Review SUS readout https://docs.google.com/presentation/d/1fJnEgCl_M2jkTzn6Z-qx_bP-9t4_cAYPg8o3P9Lad7s/edit#slide=id.gf3f626d636_0_214 
1. [x] Watch SUS Video readout https://youtu.be/wbKkGjVJBzw
1. [x] Ops PI follow up - SUS list for Verify ([Slack](https://gitlab.slack.com/archives/C0SFP840G/p1674497967465899))
1. [x] complete inside trading and compliance trainings

### Week 3

1. [x] submit comp review
1. [ ] [Talent Assessment](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2122)
    1. [x] sp
    1. [x] vm
    1. [x] km - prep doc
    1. [ ] km call - schedule
1. [x] db vm  [slack](https://gitlab.slack.com/archives/D01UAJCRF5H/p1673479210494499)
1. [x] tufts capstone
1. [ ] setup tufts project https://gitlab.com/tufts-university/tufts-university-capstone/-/issues/5 - start fri
1. [x] pto coverage issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/2192
1. [ ] add promotion task to transition issue
1. [ ] pdm handbook page updates
1. [x] release post https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/117275
1. [ ] Review Ops direction updates https://gitlab.com/gitlab-com/Product/-/issues/5332
1. [x] Ops PI updates - SUS https://gitlab.com/gitlab-com/Product/-/issues/5313
1. [ ] Follow OKRs discussion https://gitlab.com/gitlab-com/Product/-/issues/5122
1. [x] prep ux reviews ci/cd 
1. [ ] [review sj's async session plan](https://docs.google.com/document/d/1fDOOJYNv-0FvV1J5KEGypckXqckYapac4Wb8PaWNiRc/edit?pli=1) - by fri
1. [x] verify benchmarking ux alignment
1. [x] db gd, eb
1. [x] [review sus verbatim](https://gitlab.com/gitlab-org/ux-research/-/issues/2313) - by friday

### Week 2

FF day, Jan 13. out sick Jan 10-11

1. [x] propose compensation review
1. [x] update promotion docs
    1. [x] km
    1. [x] gd
    1. [x] eb
1. [x] Finish migrating source-a-thon data to GH
1. [x] Prep for Talent Assessment feedback
1. [x] talent assessment gd, eb
1. [x] Capstone - share template with G
1. [x] https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/117179+
1. [ ] https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/117275+

### Week 1 | Jan 2 - 6

Short week, OOO Jan 2-3

1. [x] Hiring!
    1. [x] Align hiring goals with Rupert
    1. [ ] ~Review GH candidates~
    1. [x] Finish migrating source-a-thon data to GH
1. [x] Review my 2023 goals
1. [x] Checkin on team members as they come back from holidays
1. [x] Ping Gina about Capstone https://gitlab.com/gitlab-org/gitlab-design/-/issues/2178+
