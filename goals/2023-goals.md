# 2023 goals

High level, off the top of my head & based on the [Product Design Manager](https://about.gitlab.com/job-families/product/product-design-management/) job description:

**product knowledge**
- stay up to date with Ops categories direction and vision. share it with the team.
- share my knowledge of ci/cd with other product design managers.

**cross-product collaboration**
- idenfity how to better connect Verify UX. set goals.
- work with the team to improve the experience of 1 flow that expands verify, package, and release.

**design quality**
- make dedicated time during the week to review in-flight ux work.
- review research studies/proposals more consistently.
- participate on beautifying our UI

**research**
- review research results on a regular basis.
- ✅ continue meeting regularly with uxr team.
- join the research shadowing program

**UX evangelism**
- ✅ establish a platform where my team can perform design critiques.
- continue collaborating with pm/em on a regular basis.
- get agreement on ux okr capacity teams need to "save" when planning key results. similar to what James and I do today.
- continue reviewing okr goals with my team.
- encourage the team to bring up initiatives they deem relevant to the dept.

**usability**
- identify, together with the team, actions we can take from SUS results. work with product to prioritize it.
- continue helping the team burn down ux-debt.

**UX process**
- understand how we can improve the community contribution review process for ux - verify.
- understand we can work with jihu - verify.
- improve team's efficiency and quality of life by addressing challenges in the ux planning and prioritization process.
- connect with the TW team to learn what can be improved and what's working well.

**hiring**
- contribute to setting the standards for the em/pm interview round - product designer hiring.
- ✅ attend conferences/events, networking.
- work with my peers to understand and improve current prep process around conducting interviews.
- write a blog post showcasing the ux dept to atract new talent.
- better understand how headcount planning works.

**career development**
- ✅ continue prioritizing the team's career development, ensuring everyone has an active growth&dev plan they are working towards.
- focus on carrer progression - work with reports that are on the promotion path to cater a measurable/actionable plan that doesn't focus on so many different areas.

**people management**
- update the ci/cd ux handbook with information that is relevant to our team and counterparts.
- measure team engagement each quarter and act on feedback.
- participate in every ux retrospective.

## Personal

- organize my calendar: meeting hours, focus time, etc.
- set measurable goals for myself: create my own career plan and discuss it with my manager.
- write 1 blog post per quarter, that will bring value to the ux organization.
- consider getting a coworking pass/subscription during summer.
- automate my weekly tasks
